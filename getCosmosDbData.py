import pydocumentdb.document_client as document_client

config = {
    'ENDPOINT': 'https://garderner.documents.azure.com',
    'MASTERKEY': '',
    'DOCUMENTDB_DATABASE': 'GardenerDb',
    'DOCUMENTDB_COLLECTION': 'gardenerCollection'
}

client = document_client.DocumentClient(config['ENDPOINT'], {'masterKey': config['MASTERKEY']})  # Initialize the Python DocumentDB client

options = {  # Create collection options
    'offerEnableRUPerMinuteThroughput': True,
    'offerVersion': "V2",
    'offerThroughput': 400
}

db_query = "select * from r where r.id = '{0}'".format(config['DOCUMENTDB_DATABASE'])  # Query DBs
db = list(client.QueryDatabases(db_query))[0]
db_link = db['_self']
coll_query = "select * from r where r.id = '{0}'".format(config['DOCUMENTDB_COLLECTION'])  # Query Collections
coll = list(client.QueryCollections(db_link, coll_query))[0]
coll_link = coll['_self']

# Query them in SQL
def getLastDbDocument():
    query = {'query': 'SELECT TOP 1 * FROM server s ORDER BY s._ts DESC'}  # Query Documents
    options = {'enableCrossPartitionQuery': True, 'maxItemCount' : 1}
    result_iterable = client.QueryDocuments(coll['_self'], query, options)
    results = list(result_iterable)
    return results[0]
