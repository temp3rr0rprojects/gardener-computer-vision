# Gardener Computer Vision #

Gardener Computer Vision: Detects a square model image in a video feed. Draws real-time sensor information & the model's translated geometry rectangle of the 3D plane. Also attaches the latest sensor/plant data from CosmosDB.

![alternativetext](test_rectange2.png)

## Techniques & algorithms
- Orb feature detector
- Hamming distance descriptor matcher
- Homography matching using RANSAC

## Algorithm Sequence

* Define the codec and create VideoWriter object
* Capture video frame-by-frame
* Convert images to gray-scale
* Detect ORB features and compute descriptors.
* Match features
* Sort matches by score
* Remove mediocre matches
* Find homography
* Use homography to warp perspective
* Display the resulting frame

![alternativetext](https://docs.opencv.org/3.4/homography_transformation_example2.jpg)

## SDKs & Libraries
- numpy
- cv2
- pydocumentdb.document_client